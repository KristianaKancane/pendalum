const watch = {
    scene: null, camera: null, renderer: null, container: null, controls: null, clock: null, stats: null, arrowHr: null, arrowMin: null, 
    arrowSec: null, timeHr: null, timeMin: null, timeSec: null, pendulum: null, pendulumBall: null, pendulum2: null, pendulumBall2: null, time: null, time2: null,
    pendulumAttr: {mass: 100, length:1000, theta: (Math.PI/2) - 0.05, omega: 0, alpha:0, J:0},
	pendulumAttr2: {mass: 100, length:1500, theta: (Math.PI/2) - 0.05, omega: 0, alpha:0, J:0},

    init() {
        this.scene = new THREE.Scene();

        var SCREEN_WIDTH = window.innerWidth,
            SCREEN_HEIGHT = window.innerHeight;

        var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 1, FAR = 15000;
        this.camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
        this.scene.add(this.camera);
        this.camera.position.set(0, 4500, 500);
        this.camera.lookAt(new THREE.Vector3(0,0,0));

        // prepare renderer
        this.renderer = new THREE.WebGLRenderer({antialias:true, alpha: false});
        this.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        this.renderer.setClearColor(0xffffff);

        this.renderer.shadowMapEnabled = true;
        this.renderer.shadowMapSoft = true;

        // prepare container
        this.container = document.createElement('div');
        document.body.appendChild(this.container);
        this.container.appendChild(this.renderer.domElement);

        // prepare controls (OrbitControls)
        this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        this.controls.target = new THREE.Vector3(0, 0, 0);

        // prepare clock
        this.clock = new THREE.Clock();


        // add dial shape
        var dialMesh = new THREE.Mesh(
            new THREE.CircleGeometry(500, 50),
            new THREE.MeshBasicMaterial({ color:0xffffff * Math.random(), side: THREE.DoubleSide })
        );
        dialMesh.rotation.x = - Math.PI / 2;
        dialMesh.position.y = 0;
        this.scene.add(dialMesh);

        // add watch rim shape
        var rimMesh = new THREE.Mesh(
          new THREE.TorusGeometry(500, 20, 10, 100),
          new THREE.MeshBasicMaterial({ color:0xffffff * Math.random() })
        );
        rimMesh.rotation.x = - Math.PI / 2;
        this.scene.add(rimMesh);

        // add watch arrow
        var iHours = 12;
        var mergedArrows = new THREE.Geometry();
        var extrudeOpts = {amount: 10, steps: 1, bevelSegments: 1, bevelSize: 1, bevelThickness:1};
        var handFrom = 400, handTo = 450;

        for (i = 1; i <= iHours; i++) {

          // prepare each arrow in a circle
          var arrowShape = new THREE.Shape();

          var from = (i % 3 == 0) ? 350 : handFrom;

          var a = i * Math.PI / iHours * 2;
          arrowShape.moveTo(Math.cos(a) * from, Math.sin(a) * from);
          arrowShape.lineTo(Math.cos(a) * from + 5, Math.sin(a) * from + 5);
          arrowShape.lineTo(Math.cos(a) * handTo + 5, Math.sin(a) * handTo + 5);
          arrowShape.lineTo(Math.cos(a) * handTo, Math.sin(a) * handTo);

          var arrowGeom = new THREE.ExtrudeGeometry(arrowShape, extrudeOpts);
          THREE.GeometryUtils.merge(mergedArrows, arrowGeom);
        }

        var arrowsMesh = new THREE.Mesh(mergedArrows, new THREE.MeshBasicMaterial({ color:0x444444 * Math.random() }));
        arrowsMesh.rotation.x = - Math.PI / 2;
        arrowsMesh.position.y = 10;
        this.scene.add(arrowsMesh);

        // add seconds arrow
        handTo = 350;
        var arrowSecShape = new THREE.Shape();
        arrowSecShape.moveTo(-50, -5);
        arrowSecShape.lineTo(Math.cos(a) * handTo, Math.sin(a) * handTo);
        arrowSecShape.lineTo(-50, 5);

        var arrowSecGeom = new THREE.ExtrudeGeometry(arrowSecShape, extrudeOpts);
        this.arrowSec = new THREE.Mesh(arrowSecGeom, new THREE.MeshBasicMaterial({ color:0x000000 }));
        this.arrowSec.rotation.x = - Math.PI / 2;
        this.arrowSec.position.y = 20;
        this.scene.add(this.arrowSec);

        // add minutes arrow
        var arrowMinShape = new THREE.Shape();
        arrowMinShape.moveTo(0, -5);
        arrowMinShape.lineTo(Math.cos(a) * handTo, Math.sin(a) * handTo - 5);
        arrowMinShape.lineTo(Math.cos(a) * handTo, Math.sin(a) * handTo + 5);
        arrowMinShape.lineTo(0, 5);

        var arrowMinGeom = new THREE.ExtrudeGeometry(arrowMinShape, extrudeOpts);
        this.arrowMin = new THREE.Mesh(arrowMinGeom, new THREE.MeshBasicMaterial({ color:0x000000 }));
        this.arrowMin.rotation.x = - Math.PI / 2;
        this.arrowMin.position.y = 20;
        this.scene.add(this.arrowMin);

        // add hours arrow
        handTo = 300;
        var arrowHrShape = new THREE.Shape();
        arrowHrShape.moveTo(0, -5);
        arrowHrShape.lineTo(Math.cos(a) * handTo, Math.sin(a) * handTo - 5);
        arrowHrShape.lineTo(Math.cos(a) * handTo, Math.sin(a) * handTo + 5);
        arrowHrShape.lineTo(0, 5);

        var arrowHrGeom = new THREE.ExtrudeGeometry(arrowHrShape, extrudeOpts);
        this.arrowHr = new THREE.Mesh(arrowHrGeom, new THREE.MeshBasicMaterial({ color:0x000000 }));
        this.arrowHr.rotation.x = - Math.PI / 2;
        this.arrowHr.position.y = 20;
        this.scene.add(this.arrowHr);

        // add pendulums

        this.pendulumAttr.J = this.pendulumAttr.mass * this.pendulumAttr.length * this.pendulumAttr.length / 500;
		this.pendulumAttr2.J = this.pendulumAttr2.mass * this.pendulumAttr2.length * this.pendulumAttr2.length / 500;
        time = new Date();
		time2 = new Date();

        var geometry = new THREE.SphereGeometry( 100, 16, 16 );
		var material = new THREE.MeshBasicMaterial( {color:0xffffff * Math.random()} );
		this.pendulumBall = new THREE.Mesh( geometry, material );
		this.pendulumBall.position.z = this.pendulumAttr.length;
		this.scene.add( this.pendulumBall );
		
		var geometry = new THREE.SphereGeometry( 100, 16, 16 );
		var material = new THREE.MeshBasicMaterial( {color:0xffffff * Math.random()} );
		this.pendulumBall2 = new THREE.Mesh( geometry, material );
		this.pendulumBall2.position.z = this.pendulumAttr2.length;
		this.scene.add( this.pendulumBall2 );
		

		this.setupEvents();
        this.animate();
    },

    setupEvents() {
		window.addEventListener( 'resize', () => {
			this.camera.aspect = window.innerWidth / window.innerHeight;
			this.camera.updateProjectionMatrix();

			this.renderer.setSize( window.innerWidth, window.innerHeight );
		}, false );
	},

	update() {
	    this.controls.update(this.clock.getDelta());

	    // get current time
	    var date = new Date;
	    this.timeSec = date.getSeconds();
	    this.timeMin = date.getMinutes();
	    this.timeHr = date.getHours();

	    // update watch arrows positions
	    var rotSec = this.timeSec * 2 * Math.PI / 60 - Math.PI/2;
	    this.arrowSec.rotation.z = -rotSec;

	    var rotMin = this.timeMin * 2 * Math.PI / 60 - Math.PI/2;
	    this.arrowMin.rotation.z = -rotMin;

	    var rotHr = this.timeHr * 2 * Math.PI / 12 - Math.PI/2;
	    this.arrowHr.rotation.z = -rotHr;
		
		// pendulum 1

	    var deltaT = (this.time - new Date().getTime()) / 1000;
	    if (deltaT > 0.050)
	    {
	        deltaT = 0.050;
	    }
	    deltaT = 0.03;

	    this.time = new Date(this.time);

	    /* Velocity Verlet */
	    /* Calculate current position from last frame's position, velocity, and acceleration */
	    this.pendulumAttr.theta += this.pendulumAttr.omega * deltaT + ( 0.5 * this.pendulumAttr.alpha * deltaT * deltaT );
	    /* Calculate forces from current position. */
	    var T = this.pendulumAttr.mass * 9.81 * Math.cos(this.pendulumAttr.theta) * this.pendulumAttr.length;
	    /* Current acceleration */
	    var alpha = T / this.pendulumAttr.J;
	    /* Calculate current velocity from last frame's velocity and 
	        average of last frame's acceleration with this frame's acceleration. */
	    this.pendulumAttr.omega += 0.5 * (alpha + this.pendulumAttr.alpha) * deltaT;
	    /* Update acceleration */
	    this.pendulumAttr.alpha = alpha;
	    var px = this.pendulumAttr.length*Math.cos(this.pendulumAttr.theta);
	    var py = Math.sin(this.pendulumAttr.theta) - 20;
		
	    this.scene.remove(this.pendulum);

	    var material = new THREE.LineBasicMaterial({
			color: 0x0000ff
		});

	    var geometry = new THREE.Geometry();
		geometry.vertices.push(
			new THREE.Vector3( 0, -20, 0 ),
			new THREE.Vector3( 0, -20, 0 ),
			new THREE.Vector3( px, py, this.pendulumAttr.length )
		);

		this.pendulum = new THREE.Line( geometry, material );
		this.scene.add(this.pendulum);
		
		this.pendulumBall.position.x = px;
		this.pendulumBall.position.y = py;
		
		
		// pendulum 2
		var deltaT = (this.time2 - new Date().getTime()) / 1000;
	    if (deltaT > 0.050)
	    {
	        deltaT = 0.050;
	    }
	    deltaT = 0.08;

	    this.time2 = new Date(this.time2);

	    /* Velocity Verlet */
	    /* Calculate current position from last frame's position, velocity, and acceleration */
	    this.pendulumAttr2.theta += this.pendulumAttr2.omega * deltaT + ( 0.5 * this.pendulumAttr2.alpha * deltaT * deltaT );
	    /* Calculate forces from current position. */
	    var T = this.pendulumAttr2.mass * 9.81 * Math.cos(this.pendulumAttr2.theta) * this.pendulumAttr2.length;
	    /* Current acceleration */
	    var alpha = T / this.pendulumAttr2.J;
	    /* Calculate current velocity from last frame's velocity and 
	        average of last frame's acceleration with this frame's acceleration. */
	    this.pendulumAttr2.omega += 0.5 * (alpha + this.pendulumAttr2.alpha) * deltaT;
	    /* Update acceleration */
	    this.pendulumAttr2.alpha = alpha;
	    var px = this.pendulumAttr2.length*Math.cos(this.pendulumAttr2.theta);
	    var py = Math.sin(this.pendulumAttr2.theta) - 20;
		
	    this.scene.remove(this.pendulum2);

	    var material = new THREE.LineBasicMaterial({
			color: 0x0000ff
		});

	    var geometry = new THREE.Geometry();
		geometry.vertices.push(
			new THREE.Vector3( 0, -20, 0 ),
			new THREE.Vector3( 0, -20, 0 ),
			new THREE.Vector3( px, py, this.pendulumAttr2.length )
		);

		this.pendulum2 = new THREE.Line( geometry, material );
		this.scene.add(this.pendulum2);
		
		
		this.pendulumBall2.position.x = px;
		this.pendulumBall2.position.y = py;
	    //this.pendulum.rotation.y = this.timeSec * 2 * Math.PI / 60 - Math.PI/2;
	},

	render() {
	    if (this.renderer) {
	        this.renderer.render(this.scene, this.camera);
	    }
	},

	animate() {
	    requestAnimationFrame(() => this.animate());
	    this.render();
	    this.update();
	}
};

watch.init();